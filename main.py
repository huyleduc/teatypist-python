import curses
from ui.terminal import welcome, exercise, results
from core import exercise_manager, statistics

def main(stdscr):
    curses.start_color()

    curses.curs_set(0)
    stdscr.keypad(True)

    chosen_exercise = welcome.display_choice(stdscr)
    exercise_text = exercise_manager.get_exercise(chosen_exercise)
    user_input, elapsed_time = exercise.display_and_capture_input(stdscr, exercise_text)
    wpm, error_rate = statistics.calculate(user_input, exercise_text, elapsed_time)
    results.display(stdscr, wpm, error_rate)

if __name__ == "__main__":
    curses.wrapper(main)
