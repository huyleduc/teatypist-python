
def calculate(user_input, exercise_text, elapsed_time):
    total_chars = len(exercise_text)

    num_words_user_input = len(user_input.split())
    mistakes = sum(1 for i, j in zip(user_input, exercise_text) if i != j)

    wpm = (num_words_user_input / (elapsed_time / 60))
    error_rate = (mistakes / total_chars) * 100

    return wpm, error_rate

