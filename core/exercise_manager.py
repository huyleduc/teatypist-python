def get_exercise(exercise_number):
    with open(f"./core/exercises/exercise{exercise_number}.txt", 'r') as f:
        return f.read()

