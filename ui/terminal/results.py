import curses

def display(stdscr, wpm, error_rate):
    stdscr.clear()
    h, w = stdscr.getmaxyx()

    wpm_text = f"Words Per Minute (WPM): {wpm:.2f}"
    error_text = f"Error Rate: {error_rate:.2f}%"

    # Determine decimal positions
    wpm_decimal_pos = wpm_text.find('.')
    error_decimal_pos = error_text.find('.')

    # Determine the maximum position of the decimal point
    max_decimal_pos = max(wpm_decimal_pos, error_decimal_pos)

    # Determine the offset for each text based on the decimal position difference
    wpm_offset = max_decimal_pos - wpm_decimal_pos
    error_offset = max_decimal_pos - error_decimal_pos

    # Calculate the center position for the text
    start_y = h // 2 - 1
    wpm_start_x = (w - len(wpm_text) - wpm_offset) // 2
    error_start_x = (w - len(error_text) - error_offset) // 2

    # Display WPM with offset
    stdscr.addstr(start_y, wpm_start_x + wpm_offset, "Words Per Minute (WPM): ", curses.A_NORMAL)
    stdscr.addstr(start_y, wpm_start_x + wpm_offset + len("Words Per Minute (WPM): "), f"{wpm:.2f}", curses.A_BOLD)

    # Display error rate with offset
    stdscr.addstr(start_y + 1, error_start_x + error_offset, "Error Rate: ", curses.A_NORMAL)
    stdscr.addstr(start_y + 1, error_start_x + error_offset + len("Error Rate: "), f"{error_rate:.2f}%", curses.A_BOLD)

    stdscr.getch()

