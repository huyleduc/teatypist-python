# ./ui/terminal/welcome.py
import curses

def display_choice(stdscr):
    choices = ["Exercise 1", "Exercise 2"]
    h, w = stdscr.getmaxyx()

    title = "Welcome to the Rich TeaTypist !"
    start_y = h // 2 - len(choices) // 2 - 1  # adjusting to center vertically based on choices count
    start_x = (w - len(title)) // 2
    selected_index = 0

    while True:
        stdscr.clear()
        stdscr.addstr(start_y - 2, start_x, title, curses.A_BOLD)  # Displaying title



        for i, choice in enumerate(choices):
            x_offset = (w - len(choice)) // 2
            if i == selected_index:
                stdscr.addstr(start_y + (i * 2), x_offset, f"{choice}", curses.A_REVERSE)  # multiplied i by 2
            else:
                stdscr.addstr(start_y + (i * 2), x_offset, choice)  # multiplied i by 2

        key = stdscr.getch()

        if key == curses.KEY_UP:
            selected_index = (selected_index - 1) % len(choices)
        elif key == curses.KEY_DOWN:
            selected_index = (selected_index + 1) % len(choices)
        elif key == curses.KEY_ENTER or key == 10:
            return selected_index + 1

