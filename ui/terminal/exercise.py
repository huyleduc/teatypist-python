import curses
import time
from collections import namedtuple

Position = namedtuple("Position", ["y", "x"])
TEXT_WIDTH = 30
LINE_DISPLAY = 3
MISTAKE_ATTRIBUTE = None

def display_and_capture_input(stdscr, exercise_text):
    init_settings()
    start_position = get_starting_position(stdscr)

    split_lines = split_text(exercise_text, TEXT_WIDTH)
    display_text(stdscr, start_position, split_lines)

    user_input, elapsed_time = capture_input(stdscr, exercise_text, start_position)

    return user_input, elapsed_time

def init_settings():
    global MISTAKE_ATTRIBUTE
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    MISTAKE_ATTRIBUTE = curses.color_pair(1) + curses.A_UNDERLINE
    curses.curs_set(1)

def get_starting_position(stdscr):
    h, w = stdscr.getmaxyx()
    return Position(h // 2 - 1, (w - TEXT_WIDTH) // 2)


def display_text(stdscr, position, split_lines, user_input='', current_line=0):
    stdscr.clear()

    split_user_input = align_user_input_with_split_lines(split_lines, user_input)

    start, end = get_line_range(split_lines, current_line)

    if len(user_input) >= sum(len(i) for i in split_lines):
        curses.curs_set(0)

    for i, line in enumerate(split_lines[start:end]):
        user_line_index = start + i
        user_line = split_user_input[user_line_index] if user_line_index < len(split_user_input) else ''
        if (i == 0 and current_line > 0) or len(user_input) >= sum(len(i) for i in split_lines):
            display_line_with_highlights(stdscr, position, user_line, i, line)
        else:
            stdscr.addstr(position.y + i, position.x, line, curses.A_DIM)

def get_line_range(split_lines, current_line):
    """Get the range of lines to display based on the user's current position."""
    start = max(current_line - 1, 0)
    end = start + LINE_DISPLAY  # Display 3 lines

    # Adjust for boundary conditions
    if current_line == 0:
        start, end = 0, min(LINE_DISPLAY, len(split_lines))
    elif current_line == len(split_lines) - 1:
        adjusted = ((LINE_DISPLAY - 1) // 2) + 1
        start, end = max(len(split_lines) - adjusted, 0), len(split_lines)

    return start, end

def display_line_with_highlights(stdscr, position, user_input, line_index, line):
    for j, char in enumerate(line):
        if j  < len(user_input):
            if user_input[j] != char:
                stdscr.addch(position.y + line_index, position.x + j, char, MISTAKE_ATTRIBUTE )
            else:
                stdscr.addch(position.y + line_index, position.x + j, char)
        else:
            stdscr.addch(position.y + line_index, position.x + j, char, curses.A_DIM)



def capture_input(stdscr, exercise_text,  position ):
    stdscr.move(position.y, position.x)
    user_input = ''
    idx = 0
    line_index = 0
    start_time, elapsed_time = None, 0
    split_words = split_text(exercise_text, TEXT_WIDTH)

    while idx < len(exercise_text):
      if  start_time is None :
          start_time = time.time()

      idx, user_input = process_key(stdscr, idx, user_input, exercise_text, position)

      if idx - sum(len(split_words[i]) for i in range(line_index)) >= len(split_words[line_index]):
        line_index += 1
        line_index = min(line_index, len(split_words) - 1)
        display_text(stdscr, position, split_words, user_input, line_index)
        stdscr.move(position.y+1, position.x)

      if len(user_input) == len(exercise_text):
          elapsed_time = time.time() - start_time
          prompt_for_results(stdscr)

    return user_input, elapsed_time

def split_text(text, text_width):
    words = text.split()
    result, current_line = [], []
    current_length = 0

    for word in words:
        # Check if word can be added to the current line
        if current_length + len(word) <= text_width:
            current_line.append(word)
            current_length += len(word) + 1  # +1 for the space
        else:
            # Add the current line to the result and start a new line
            result.append(' '.join(current_line) + ' ')
            current_line = [word]
            current_length = len(word) + 1

    # Add any remaining words in the current line to the result
    if current_line:
        result.append(' '.join(current_line))

    return result

def align_user_input_with_split_lines(split_lines, user_input):
    aligned_input = []
    start_idx = 0

    for line in split_lines:
        end_idx = start_idx + len(line)
        aligned_input.append(user_input[start_idx:end_idx])
        start_idx = end_idx

    return aligned_input + [''] * (len(split_lines) - len(aligned_input))

def process_key(stdscr, idx, user_input, exercise_text, position):
    c = stdscr.getch()
    if c == curses.KEY_BACKSPACE and idx > 0:
        idx -= 1
        stdscr.addch(exercise_text[idx], curses.A_DIM)
        stdscr.move(position.y, position.x + idx)
        user_input = user_input[:-1]
    elif c == ord(exercise_text[idx]):
        stdscr.addch(c)
    else:
        stdscr.addch(exercise_text[idx], MISTAKE_ATTRIBUTE)
    if len(user_input) < len(exercise_text):
      user_input += chr(c)
      idx += 1
    return idx, user_input

def prompt_for_results(stdscr):
    h, w = stdscr.getmaxyx()
    msg = "Press Enter to see results..."
    stdscr.addstr(h - 1, (w- len(msg)) // 2 , msg, curses.A_BOLD)
    while True:
        key = stdscr.getch()
        if key in [curses.KEY_ENTER, 10]:
            break
